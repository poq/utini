export default class Databank {

    constructor () {

        this.data = new Map()

    }


    ingest (groups) {

        if (!groups) return

        this.data.clear()
        let activeGroups = groups.filter(g => g.active && !g.editing)

        activeGroups.forEach(group => {

            let frozen = group.routes.filter(r => !r.editing)
            frozen.forEach(r => this.data.set(r.src, r))

        })

    }


    get (route) {

        return this.data.get(route)

    }


}
