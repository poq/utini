export default function MasterControl (cluster) {

    let app = require('app')
    let dispatch = require('electron').ipcMain
    let BrowserWindow = require('browser-window')
    let crashReporter = require('crash-reporter')
    let proxyProcess = cluster.fork()
    let window = null

    crashReporter.start({

        companyName: 'utini',
        submitURL: 'https://github.com/deomitrus/utini'

    })


    dispatch.on('routes', (ipcEvent, routes) => {

        proxyProcess.send({

            event: 'routes',
            payload: routes

        })

    })


    app.on('window-all-closed', () => {

        if (process.platform !== 'darwin')
            app.quit()

    })


    app.on('ready', () => {

        window = new BrowserWindow({

            width: 1000,
            height: 800
            //transparent: true,
            //frame: false

        })

        window.loadURL('file://' + __dirname + '/../ui/index.html')
        window.openDevTools() // debug only
        window.on('closed', () => window = null)

        // Forward messages from proxy to render process
        proxyProcess.on('message', data => {

            if (data.event && data.payload)
                window.webContents.send(data.event, data.payload)

        })

    })

    cluster.on('exit', (worker, code, signal) =>
        proxyProcess = cluster.fork())

}
