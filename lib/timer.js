export default class Timer {

    constructor () {

        let startTime = 0
        let endTime = 0

        this.start = () => {

            startTime = process.hrtime()
            return this

        }

        this.end = () => {

            endTime = process.hrtime()
            return this

        }

        this.elapsed = () => {

            let startE = startTime[0] * 1e9 + startTime[1]
            let endE = endTime[0] * 1e9 + endTime[1]

            return Math.ceil((endE-startE) / 1000000)

        }

        return this

    }

}
