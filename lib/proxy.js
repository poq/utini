import express from 'express'
import httpProxy from 'http-proxy'
import uuid from 'node-uuid'
import Timer from './timer'
import Databank from './databank'


export default class Proxy {

    constructor () {

        console.log('proxy has started')

        this.proxy = httpProxy.createServer()
        this.app = express()
        this.app.disable('x-powered-by')
        this.db = new Databank()
        this.routes = {} // deprecate pls
        this.active = ''
        this.dispatch = {

            emit: (ev, load) => process.send({ event: ev, payload: load })

        }

        process.on('message', data => {

            if (data.payload && data.event === 'routes')
                this.db.ingest(data.payload)

        })

    }


    listen (port) {

        this.app.listen(port, () => {

            this.dispatch.emit('proxy-started', {

                online: true,
                port: port

            })

        })


        this.app.all('*', this._handleUrl.bind(this))


        this.proxy.on('proxyReq', (request, req, res, options) => {

            request.utiniId = uuid.v4()
            req.utiniId = request.utiniId
            request.utiniTimer = new Timer().start()

            this.dispatch.emit('proxy-request', {

                id: request.utiniId,
                event: 'request',
                url: req.url,
                headers: req.headers,
                method: req.method

            })

        })


        this.proxy.on('proxyRes', (response, req, res) => {

            this.dispatch.emit('proxy-response', {

                id: response.req.utiniId,
                event: 'response',
                duration: response.req.utiniTimer.end().elapsed(),
                statusCode: response.statusCode,
                statusMessage: response.statusMessage,
                headers: response.headers

            })

        })


        this.proxy.on('error', (error, req, res, options) => {

            this.dispatch.emit('proxy-error', {

                id: req.utiniId,
                event: 'error',
                error: error

            })

        })

    }


    _handleUrl (req, res) {

        let url = req.url
        let host = req.headers.host

        this._forward(req, res, this._matchUrl(url, host))

    }


    _matchUrl (url, host) {

        if (this._existsRoute(url) || this._existsRoute(host)) {

            return this._getRoute(url) || this._getRoute(host)

        } else {

            return { dest: url }

        }

    }


    _forward (req, res, route) {

        if (route.type === 'file') {

            res.sendFile(route.dest)

        } else {

            this.proxy.web(req, res, { [ route.type || 'target' ]: route.dest })

        }

    }


    _existsRoute (source) {

        return this.routes[this.active] && this.routes[this.active][source]

    }


    _getRoute (source) {

        return this.routes[this.active] ? this.routes[this.active][source] || null : null

    }

}
