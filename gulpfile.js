'use strict'

const gulp = require('gulp')
const concat = require('gulp-concat')
const babel = require('gulp-babel')
const sourcemaps = require('gulp-sourcemaps')
const stylus = require('gulp-stylus')
const del = require('del')
const livereload = require('gulp-livereload')


const files = {

    app: 'app.js',
    ui: 'ui/*.js',
    lib: 'lib/*.js',
    styles: 'ui/main.styl',
    substyles: 'ui/styles/*.styl',
    templates: 'ui/templates/*.html',
    components: 'ui/components/*.js',
    staticAssets: [

        'ui/index.html',
        'ui/fonts.css',
        'ui/fonts/**',
        'semantic/dist/semantic.min.css',
        'semantic/dist/semantic.min.js'

    ],
    icons: [

        'semantic/dist/themes/default/assets/fonts/icons.*',

    ]

}


let logBabelError = (err, stream) => {

    console.log(err.codeFrame)
    console.log(err.message)

}


gulp.task('ui', () => {

    let bb = babel()
    bb.on('error', logBabelError.bind(bb))

    return gulp.src(files.ui)
        .pipe(sourcemaps.init())
        .pipe(bb)
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/ui'))
        .pipe(livereload())

})


gulp.task('lib', () => {

    let bb = babel()
    bb.on('error', logBabelError.bind(bb))

    return gulp.src(files.lib)
        .pipe(sourcemaps.init())
        .pipe(bb)
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/lib'))
        .pipe(livereload())

})


gulp.task('copy', () => {

    return gulp.src(files.staticAssets)
        .pipe(gulp.dest('dist/ui'))
        .pipe(livereload())

})


gulp.task('components', () => {

    let bb = babel()
    bb.on('error', logBabelError.bind(bb))

    return gulp.src(files.components)
        .pipe(sourcemaps.init())
        .pipe(bb)
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/ui/components'))
        .pipe(livereload())

})


gulp.task('templates', () => {

    return gulp.src(files.templates)
        .pipe(concat('templates.html'))
        .pipe(gulp.dest('dist/ui'))
        .pipe(livereload())

})


gulp.task('app', () => {

    return gulp.src(files.app)
        .pipe(gulp.dest('dist/'))
        .pipe(livereload())

})


gulp.task('styles', () => {

    return gulp.src(files.styles)
        .pipe(sourcemaps.init())
        .pipe(stylus())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/ui'))
        .pipe(livereload())

})


gulp.task('icons', () => {

    return gulp.src(files.icons)
        .pipe(gulp.dest('dist/ui/themes/default/assets/fonts'))
        .pipe(livereload())

})


// TODO: on watch need to clean only stuff that changed, not entire

gulp.task('clean', () => {

    return del('dist/**')

})


gulp.task('watch', () => {

    livereload.listen({

        port: 9009,
        quiet: true

    })

    gulp.watch(files.ui, ['ui'])
    gulp.watch(files.lib, ['lib'])
    gulp.watch(files.staticAssets, ['copy'])
    gulp.watch(files.app, ['app'])
    gulp.watch(files.styles, ['styles'])
    gulp.watch(files.substyles, ['styles'])
    gulp.watch(files.components, ['components'])
    gulp.watch(files.templates, ['templates'])

})


gulp.task('build', [

    'app',
    'lib',
    'styles',
    'components',
    'templates',
    'icons',
    'ui',
    'copy'

])
