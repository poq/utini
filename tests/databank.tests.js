'use strict'

const expect = require('chai').expect
const sinon = require('sinon')
const Databank = require('../dist/lib/databank')


describe('Databank', function () {

    beforeEach(() => {

        this.db = new Databank()

        this.groups = [{

            name: 'main',
            id: 123,
            active: true,
            editing: true,
            routes: [{
                editing: false,
                src: 'ok',
                dest: 'nop'
            }]

        }, {

            name: 'another',
            id: 234,
            active: true,
            editing: false,
            routes: [{
                editing: true,
                src: 'pizza',
                dest: 'lasagna'
            }, {
                editing: false,
                src: 'spaghetti',
                dest: 'ciabatta'
            }]

        }, {

            name: 'not me',
            id: 909,
            active: false,
            editing: false,
            routes: [{
                editing: false,
                src: 'lol',
                dest: 'umad'
            }]

        }]

    })


    it('should get one of the ingested routes', () => {

        this.db.ingest(this.groups)
        let res = this.db.get('spaghetti')

        expect(res).to.be.an('object')
        expect(res.src).to.equal('spaghetti')
        expect(res.dest).to.equal('ciabatta')

    })


    it('should NOT get routes when group is inactive', () => {

        this.db.ingest(this.groups)
        let res = this.db.get('lol')

        expect(res).to.not.exist

    })


    it('should NOT get routes when route is being edited', () => {

        this.db.ingest(this.groups)
        let res = this.db.get('pizza')

        expect(res).to.not.exist

    })


    it('should NOT get routes when group is being edited', () => {

        this.db.ingest(this.groups)
        let res = this.db.get('ok')

        expect(res).to.not.exist

    })

})
