window.$ = require('jquery')
window.jQuery = require('jquery')
const semantic = require('./semantic.min.js')
const dispatch = require('electron').ipcRenderer

// Load templates
let templates = $($('#templates')[0].import).find('template')
$('body').append(templates)

import Vue from 'vue'
import directives from './directives.js'
import transitions from './transitions.js'
import activity from './components/activity.js'
import groups from './components/groups.js'
import routes from './components/routes.js'
import menuItem from './components/menuItem.js'


Vue.config.debug = true
const ACTIVITY_LIMIT = 1000


let holocron = {

    active: 'activity',
    groups: [],
    activities: {}

}


dispatch.on('proxy-request', (e, req) => {

    console.log('req:', req)

    holocron.activities[req.id] = {

        url: req.url,
        reqHeaders: req.headers,
        method: req.method

    }

})


dispatch.on('proxy-response', (e, res) => {

    console.log('res:', res)

    Object.assign(holocron.activities[res.id] || {}, {

        status: res.statusCode,
        duration: res.duration,
        resHeaders: res.headers

    })

})


let app = new Vue({

    el: '#galaxy',

    data: holocron,

    components: {

        activity,
        groups,
        routes,
        menuItem

    },

    methods: {

        activate (thing) {

            this.active = thing

        },

        is (thing) {

            return this.active.toUpperCase() === thing.toUpperCase()

        }

    }

})

let unvue = function (model) {
    return JSON.parse(JSON.stringify(model))
}

let prepare = function (groups) {

    let clean = unvue(groups).filter(g => g.active && !g.editing)
    clean.forEach(g => g.routes = g.routes.filter(r => !r.editing))

    return clean.filter(g => g.routes && g.routes.length)

}

// Soon™
//app.$watch('groups', g => console.log('g is', unvue(g)), { deep: true })

app.$watch('groups', groups => {

    let parcel = prepare(groups)

    if (parcel && parcel.length)
        dispatch.send('routes', parcel)

}, { deep: true })
