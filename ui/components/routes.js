import groupListItem from './groupListItem.js'
import routeListItem from './routeListItem.js'


let methods = {

    getActiveGroup () {

        return this.groups.find(g => g.id === this.activeGroup)

    },

    getActiveGroupRoutes () {

        let group = this.getActiveGroup()
        return group ? group.routes : []

    },


    addRoute () {

        let group = this.getActiveGroup()
        if (!group) return

        group.routes.unshift({

            src: '',
            dest: '',
            editing: true

        })

    },

    removeRoute (route) {

        let group = this.getActiveGroup()
        if (!group) return

        group.routes.$remove(route)

    },

    groupNameForId (id) {

        let group = this.groups.find(g => g.id === id)
        return group ? group.name || id : id

    },

    selectGroup (id) {
        this.activeGroup = id
    },

    addGroup () {

        this.groups.unshift({

            id: Date.now(),
            name: '',
            routes: [],
            editing: true,
            active: true // TODO: default is false

        })

    },

    removeGroup (group) {
        this.groups.$remove(group)
    }

}


module.exports = {

    template: '#ut-routes',

    props: [

        'groups',
        'routes'

    ],

    components: {

        'group-list-item': groupListItem,
        'route-list-item': routeListItem

    },

    data: () => ({

        activeGroup: '',
        groupFilter: ''

    }),

    methods

}
/*
                <search-dropdown
                    placeholder="Filter groups"
                    :value.sync="groupFilter"
                    :options="groups"
                    option-text="name"
                    option-value="id"
                ></search-dropdown>
*/
