module.exports = {

    template: '#ut-group-list-item',

    props: [

        'group'

    ],

    data () {

        return {
            name: this.group.name
        }

    },

    methods: {

        save () {
            this.group.editing = false
            this.group.name = this.name
        },

        remove () {
            this.$dispatch('remove-group', this.group)
        }

    }

}
