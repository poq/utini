module.exports = {

    template: '#ut-route-list-item',

    props: [

        'route'

    ],

    data () {

        return {
            src: this.route.src,
            dest: this.route.dest
        }

    },

    methods: {

        save () {
            this.route.editing = false
            this.route.src = this.src
            this.route.dest = this.dest
        },

        remove () {
            this.$dispatch('remove-route', this.route)
        }

    }

}
