module.exports = {

    data () {
        return { blarg: 1 }
    },

    template: '#search-dropdown',

    props: [

        'value',
        'options',
        'optionText',
        'optionValue',
        'placeholder'

    ],

    directives: {

        'dropdown': {

            bind () {

                setTimeout(() => $(this.el).dropdown())

            },

            update (val) {

                if (typeof val == 'object')
                    setTimeout(() => $(this.el).dropdown('restore defaults'))

            }

        }

    }

}
