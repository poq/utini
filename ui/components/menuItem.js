module.exports = {

    template: '#ut-menu-item',

    props: [

        'label',
        'icon'

    ],

    data: () => ({}),

    methods: {

        activate () {

            this.$dispatch('activate', this.label)

        }

    }

}
