import groupBox from './groupBox.js'


module.exports = {

    template: '#ut-groups',

    props: [

        'groups'

    ],

    components: {

        'group-box': groupBox

    },

    data: () => ({})

}
