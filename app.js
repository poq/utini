var cluster = require('cluster')
var Proxy = require('./lib/proxy.js')
var MasterControl = require('./lib/master.js')


if (cluster.isMaster) {

    MasterControl(cluster)

}

else {

    var proxy = new Proxy()
    proxy.listen(9001)

}
